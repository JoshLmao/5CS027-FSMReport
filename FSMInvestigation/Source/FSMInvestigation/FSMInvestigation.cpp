// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FSMInvestigation.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FSMInvestigation, "FSMInvestigation" );

DEFINE_LOG_CATEGORY(LogFSMInvestigation)
 